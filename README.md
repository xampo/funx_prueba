funx_prueba

primera parte

Para ejecutar el proyecto, primero se debe tener instalado ruby on rails,
Las app se crearon con ruby 2.6.3 y rails 6.0.

al momento de copiar las app en un computador local, debe ir a la carpeta principal
de cada proyecto y ejecutar el comando `bundle install`.

ejecutar el comando `rake db:setup` para crear la base de datos

ejecutar el comando `rake db:migrate`


1.- correr el servidor de la app funx-api con el comando `rails s`

2.- verificar que se levantó en el puerto 3000

3.- correr el servidor de la app funx-front con el comando `rails s -p 3003`

4.- verificar que se levantó en el puerto 3003

Se Asumió lo siguiente:

- el top de artistas siempre es el top 10.
- quien en trega el top 10 es el API.
- la app funx-front no almacena información.
- se asume que los datos ingresados por el formulario sean válidos.
- no se agregará paginador a los endpoint index.
- no se agregará login ni sistema de autenticación.
- los datos se ingresarán de forma manual.


segunda parte

ir a la carpeta que donde se encuentra el archivo factorial.rb
para ejecutar, introducir el comando
`ruby factorial.b <numero>`
