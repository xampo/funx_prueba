class TopsController < ApplicationController

  def top
    @artists = Top.top_ten
  end
end
