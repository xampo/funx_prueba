module ApiClient
  extend ActiveSupport::Concern

  included do
    require 'ostruct'

    MAIN_URL = 'localhost:3000/api/v1'.freeze

    def self.get_json(url)
      request_json(url, :get)
    end

    def self.post_json(url, params)
      request_json(url, :post, params)
    end

    def self.put_json(url, params)
      request_json(url, :put, params)
    end

    def self.delete_json(url)
      RestClient.try(:delete, url)
    end

    private
    def self.request_json(url, method, params = {})
      response = RestClient.try(method, url, params)
      JSON.parse(response.body, object_class: OpenStruct)
    end

  end
end