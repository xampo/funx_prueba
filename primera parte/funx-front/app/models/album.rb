class Album
  include ApiClient

  ALBUM_BASE = '/albums'.freeze

  attr_accessor :id, :name, :reproductions, :artist_id

  def initialize(params = {})
    self.id = params[:id]
    self.name = params[:name]
    self.reproductions = params[:reproductions]
    self.artist_id = params[:artist_id]
  end

  def self.all
    get_json("#{MAIN_URL}#{ALBUM_BASE}")
  end

  def self.find(album_id)
    Album.new(get_json("#{MAIN_URL}#{ALBUM_BASE}/#{album_id}"))
  end

  def save
    response = Album.post_json("#{MAIN_URL}#{ALBUM_BASE}", album_params)
    self.id = response.id
  end

  def update(params)
    Album.put_json("#{MAIN_URL}#{ALBUM_BASE}/#{id}", { album: params.to_h })
  end

  def destroy
    Album.delete_json("#{MAIN_URL}#{ALBUM_BASE}/#{id}")
  end

  private

  def album_params
    {album: {id: id, name: name, reproductions: reproductions, artist_id: artist_id}}
  end


  #post_json("#{MAIN_URL}#{ALBUM_BASE}", album_params)

end
