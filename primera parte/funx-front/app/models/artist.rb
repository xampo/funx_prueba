class Artist
  include ApiClient

  BASE = '/artists'.freeze

  attr_accessor :id, :name

  def initialize(params = {})
    self.id = params[:id]
    self.name = params[:name]
  end

  def self.all
    get_json("#{MAIN_URL}#{BASE}")
  end

  def self.find(artist_id)
    Artist.new(get_json("#{MAIN_URL}#{BASE}/#{artist_id}"))
  end

  def save
    response = Artist.post_json("#{MAIN_URL}#{BASE}", artist_params)
    self.id = response.id
  end

  def update(params)
    Artist.put_json("#{MAIN_URL}#{BASE}/#{id}", { artist: params.to_h })
  end

  def destroy
    Artist.delete_json("#{MAIN_URL}#{BASE}/#{id}")
  end

  private

  def artist_params
    {artist: {id: id, name: name}}
  end
end
