class Top
  include ApiClient
  BASE = '/top'.freeze
  def self.top_ten
    get_json("#{MAIN_URL}#{BASE}")
  end
end
