Rails.application.routes.draw do

  root "tops#top"

  resources :albums
  resources :artists
  get 'top', to: 'tops#top'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
