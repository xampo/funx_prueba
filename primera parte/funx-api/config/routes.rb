Rails.application.routes.draw do

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api do
    namespace :v1 do
      resources :artists
      resources :albums
      get 'top', to: 'tops#top'
    end
  end
end
