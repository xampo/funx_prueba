class CreateAlbums < ActiveRecord::Migration[6.0]
  def change
    create_table :albums do |t|
      t.string :name
      t.integer :reproductions
      t.belongs_to :artist, foreign_key: true

      t.timestamps
    end
  end
end
