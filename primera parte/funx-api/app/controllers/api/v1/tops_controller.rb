class Api::V1::TopsController < ApplicationController

  def top
    @artists = Artist.limit(10).top_reproductions_ordered

    render json: @artists
  end
end
