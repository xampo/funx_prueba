class Artist < ApplicationRecord
  has_many :albums
  validates_presence_of :name

  scope :top_reproductions_ordered, -> { joins(:albums).group('artists.id').order('SUM(reproductions) desc').select("SUM(reproductions) AS reproductions, artists.name" ) }
end
