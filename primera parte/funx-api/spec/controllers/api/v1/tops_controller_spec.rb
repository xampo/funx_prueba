require 'rails_helper'

RSpec.describe Api::V1::TopsController, type: :controller do

  describe "GET #top" do
    it "returns a success response" do
      artist = Artist.create!(name: 'Artist 1')
      Album.create!(name: 'Album-1', reproductions: 1, artist: artist)
      get :top, params: {}
      expect(response).to be_successful
    end
  end

end
