require 'rails_helper'

RSpec.describe Artist, type: :model do
   before(:each) do
    @artist = Artist.create(name: 'Artist 1')
    @album = Album.create(name: 'Album 1', reproductions: 1, artist: @artist)

    @artist2 = Artist.create(name: 'Artist 2')
    @album2 = Album.create(name: 'Album 2', reproductions: 2, artist: @artist2)

  end
  it "filter ordered by reproductions" do
    # Using the shortened version of FactoryGirl syntax.
    # Add:  "config.include FactoryGirl::Syntax::Methods" (no quotes) to your spec_helper.rb
    expect(Artist.top_reproductions_ordered.first.name).to eq(@artist2.name)
  end
end
