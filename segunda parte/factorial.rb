# ejecutar de la siguiente manera
# ruby factorial.rb <numero>

def factorial(n)
  return 1 if n <= 0

  (1..n).inject(:*)
end

puts factorial(ARGV[0].to_i)